import React from 'react';
import SidebarElement from './SidebarElement/SidebarElement';
import homeIcon from '../../assets/home-icon-silhouette.svg';
import catalogIcon from '../../assets/sports-car.svg';

import './Sidebar.css';

const header = () => {
  const elements = [
    {id: 1, title: 'Home', link: '/', img: homeIcon},
    {id: 2, title: 'Catalog', link: '/catalog', img: catalogIcon}
  ];
  return(
    <nav className='sidebar'>
      {elements.map(el => <SidebarElement key={el.id} link={el.link} title={el.title} img={el.img} />)}
    </nav>
  );
};

export default header;