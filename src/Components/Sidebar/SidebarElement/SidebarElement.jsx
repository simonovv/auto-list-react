import React from 'react';
import { NavLink } from 'react-router-dom';

import './SidebarElement.css';

const sidebarElement = (props) => (
  <div className='sidebar-element'>
    <NavLink to={props.link}>
      <img
        className='sidebar-element-image'
        src={props.img}
        alt={props.title}/>
    </NavLink>
  </div>
);

export default sidebarElement;