import React from 'react';

import './CarListItem.css';

const carListItem = props => {
  const { id, mark, name, engine, color, volume, img, togleEditingHandler, price } = props;
  return(
    <div className='list-item' onClick={() => togleEditingHandler(id)}>
      <img
        className='list-item-image'
        src={img}
        alt='Car preview'/>
      <p className='item-name'>{mark} {name}</p>
      <div className='item-color'>
        <p>Color: </p><div style={{backgroundColor: color, width: '20px', height: '20px', marginLeft: '16px'}}></div>
      </div>
      <ul>
        <li>Engine: {engine}</li>
        <li>Engine volume: {volume}</li>
      </ul>
      <p className='item-price'>Price: {price}$</p>
    </div>
  );
};

export default carListItem;