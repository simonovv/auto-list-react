import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Sidebar from './Components/Sidebar/Sidebar';
import Shop from './Containers/Shop-scene/Shop-scene';

import './Layout.css';

class Layout extends Component {
  render() {
    return(
      <div className='layout'>
        <Sidebar />
        <Route path='/catalog' component={Shop}/>
      </div>
    );
  }
}

export default Layout;