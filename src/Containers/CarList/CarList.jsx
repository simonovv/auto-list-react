import React, { Component } from 'react';
import { connect } from 'react-redux';
import CarListItem from '../../Components/CarListItem/CarListItem';
import EditingCarItem from '../EditingCarItem/EditingCarItem';
import { togleEditing } from '../../store/actions/cars';

import './CarList.css'

class CarList extends Component {
  togleEditingHandler = id => {
    const { toggleEditing } = this.props;
    toggleEditing(id);
  }

  //sorting cars by their mark and name
  render() {
    const { cars, selectedMark, selectedName } = this.props;
    return(
      <div className='car-list'>
        {cars && cars.map(el => {
          if(el.isEditing) {
            return <EditingCarItem
                      key={el.id}
                      id={el.id}
                      mark={el.mark}
                      name={el.name}
                      engine={el.engine}
                      color={el.color}
                      volume={el.volume}
                      img={el.img}
                      price={el.price}
                      togleEditingHandler={this.togleEditingHandler}/>
          }
          else {
            if(selectedMark !== 'All' || selectedName !== 'All') {
              if((selectedMark === 'All' && selectedName === el.name) || (selectedMark === el.mark && selectedName === 'All')) {
                return <CarListItem
                          key={el.id}
                          id={el.id}
                          mark={el.mark}
                          name={el.name}
                          engine={el.engine}
                          color={el.color}
                          volume={el.volume}
                          img={el.img}
                          price={el.price}
                          togleEditingHandler={this.togleEditingHandler} />
              }
              else if(selectedMark === el.mark && selectedName === el.name) {
                return <CarListItem
                          key={el.id}
                          id={el.id}
                          mark={el.mark}
                          name={el.name}
                          engine={el.engine}
                          color={el.color}
                          volume={el.volume}
                          img={el.img}
                          price={el.price}
                          togleEditingHandler={this.togleEditingHandler} />
              }
              else 
                return null;
            }
            else 
              return <CarListItem
                        key={el.id}
                        id={el.id}
                        mark={el.mark}
                        name={el.name}
                        engine={el.engine}
                        color={el.color}
                        volume={el.volume}
                        img={el.img}
                        price={el.price}
                        togleEditingHandler={this.togleEditingHandler}/>
          }
        }) }
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  toggleEditing: id => dispatch(togleEditing(id)),
});

const mapStateToProps = state => ({
  cars: state.cars.cars,
  selectedMark: state.cars.selectedMark,
  selectedName: state.cars.selectedName,
});

export default connect(mapStateToProps, mapDispatchToProps)(CarList);