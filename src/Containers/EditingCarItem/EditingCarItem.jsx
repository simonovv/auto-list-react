import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateCarInfo, togleEditing } from '../../store/actions/cars';

import './EditingCarItem.css';

class EdditingCarItem extends Component {
  state = {
    updateCar: {
      mark: {
        value: this.props.mark,
        touched: false,
        validation: {
          required: true,
          minLength: 5,
          maxLength: 20
        }
      },
      name: {
        value: this.props.name,
        touched: false,
        validation: {
          required: true,
          minLength: 5,
          maxLength: 20
        }
      },
      engine: {
        value: this.props.engine,
        touched: false,
        validation: {
          required: true
        }
      },
      color: {
        value: this.props.color,
        touched: false,
        validation: {
          required: true
        }
      },
      volume: {
        value: this.props.volume,
        touched: true,
        validation: {
          required: true
        }
      },
      price: {
        value: this.props.price,
        touched: false,
        validation: {
          min: 0,
          max: 99999
        }
      }
    }
  }

  typeHandler = event => {
    event.stopPropagation();
    const { target: { name, value }} = event;
    this.setState(prev => ({
      ...prev,
      updateCar: {
        ...prev.updateCar,
        [name]: {
          ...prev.name,
          touched: true,
          value: value
        }
      }
    }))
  }

  stopPropagation = event => {
    event.stopPropagation();
  }

  updateCarInfoHandler = () => {
    const { updateCarInfo, id, togleEditing, img } = this.props;
    const { updateCar: { color, mark, name, engine, volume, price } } = this.state;
    updateCarInfo(id, {
      mark: mark.value,
      name: name.value,
      color: color.value,
      volume: volume.value,
      engine: engine.value,
      img,
      price: price.value,
    })
    togleEditing(id);
  }

  render() {
    const { updateCar: { name, mark, color, engine, volume, price } } = this.state;
    const { id, togleEditingHandler, img } = this.props;
    return(
      <div
        className='list-item'
        onClick={() => togleEditingHandler(id)}
      >
        <img
          className='list-item-image'
          src={img}
          alt='Car preview'
        />
        <input
          className='editing-item-mark'
          value={mark.value}
          onChange={this.typeHandler}
          onClick={this.stopPropagation}
          name='mark'
        />
        <input
          className='editing-item-name'
          value={name.value}
          onChange={this.typeHandler}
          onClick={this.stopPropagation}
          name='name'
        />
        <div className='item-color'>
          <p>Color: </p>
          <input
            className='item-color'
            value={color.value}
            onChange={this.typeHandler}
            onClick={this.stopPropagation}
            name='color'
          />
        </div>
        <ul className='list'>
          <li>Engine: </li>
          <input
            value={engine.value}
            onChange={this.typeHandler}
            onClick={this.stopPropagation}
            name='engine'
          />
          <li>Engine volume: </li><input
            value={volume.value}
            onChange={this.typeHandler}
            onClick={this.stopPropagation}
            name='volume'
          />
          <li>Price: </li><input
            value={price.value}
            onChange={this.typeHandler}
            onClick={this.stopPropagation}
            name='price'
          />
        </ul>
        <button className='save-btn' onClick={this.updateCarInfoHandler}>Update</button>
      </div>
    );
  };
}

const mapDispatchToProps = dispatch => ({
  updateCarInfo: (id, car, handler) => dispatch(updateCarInfo(id, car, handler)),
  togleEditing: id => dispatch(togleEditing(id)),
});

export default connect(null, mapDispatchToProps)(EdditingCarItem);