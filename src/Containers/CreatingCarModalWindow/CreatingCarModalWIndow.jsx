import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addNewCar, togleModal } from '../../store/actions/cars';

import './CreatingCarModalWindow.css';

class CreatingCarModal extends Component {
  state = {
    newCar: {
      mark: {
        value: '',
        touched: false,
        validation: {
          required: true,
          minLength: 5,
          maxLength: 20
        }
      },
      name: {
        value: '',
        touched: false,
        validation: {
          required: true,
          minLength: 5,
          maxLength: 20
        }
      },
      engine: {
        value: '',
        touched: false,
        validation: {
          required: true
        }
      },
      color: {
        value: '',
        touched: false,
        validation: {
          required: true
        }
      },
      volume: {
        value: '',
        touched: true,
        validation: {
          required: true
        }
      },
      price: {
        value: '',
        touched: true,
        validation: {
          required: true,
          min: 0,
          max: 99999
        }
      }
    }
  }

  typeHandler = event => {
    const { target: { name, value }} = event;
    this.setState(prev => ({
      ...prev,
      newCar: {
        ...prev.newCar,
        [name]: {
          ...prev.name,
          touched: true,
          value: value
        }
      }
    }))
  }

  togleModalHandler = () => {
    const { togleModal } = this.props;
    togleModal();
  }

  stopPropagation = event => {
    event.stopPropagation();
  }

  createNewCarHandler = () => {
    const img = document.querySelector('.uploadImg').files[0];
    if(img) {
      var path = (window.URL || window.webkitURL).createObjectURL(img);
    }
    const { newCar: { name, mark, color, engine, volume, price } } = this.state;
    const { createCar } = this.props;
    const newCar = {
      name: name.value,
      mark: mark.value,
      color: color.value,
      engine: engine.value,
      volume: volume.value,
      img: path || 'https://www.cstatic-images.com/car-pictures/xl/USC70CHC021E021001.jpg',
      price: price.value,
    }
    createCar(newCar);
    this.togleModalHandler();
  }

  render() {
    const { newCar: { mark, name, volume, engine, color, price } } = this.state;
    return(
      <div onClick={this.togleModalHandler} className='backdrop'>
        <div className='modal' onClick={this.stopPropagation}>
          <h3>Creating car</h3>
          <div className='modal-element'>
            <p>Mark: </p><input value={mark.value} name='mark' onChange={this.typeHandler}/>
          </div>
          <div className='modal-element'>
            <p>Name: </p><input value={name.value} name='name' onChange={this.typeHandler}/>
          </div>
          <div className='modal-element'>
            <p>Engine: </p><input value={engine.value} name='engine' onChange={this.typeHandler}/>
          </div>
          <div className='modal-element'>
            <p>Color: </p><input value={color.value} name='color' onChange={this.typeHandler}/>
          </div>
          <div className='modal-element'>
            <p>Volume: </p><input value={volume.value} name='volume' onChange={this.typeHandler}/>
          </div>
          <div className='modal-element'>
            <p>Price: </p><input value={price.value} name='price' onChange={this.typeHandler}/>
          </div>
          <input type='file' className='uploadImg'/>
          <button onClick={this.createNewCarHandler}>Create</button>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  createCar: (car) => dispatch(addNewCar(car)),
  togleModal: () => dispatch(togleModal())
})

export default connect(null, mapDispatchToProps)(CreatingCarModal);