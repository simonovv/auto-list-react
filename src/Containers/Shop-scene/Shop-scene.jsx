import React, { Component } from 'react';
import CarList from '../CarList/CarList';
import Controls from '../Controls/Controls';
import CreatingCarModal from '../CreatingCarModalWindow/CreatingCarModalWIndow';
import { connect } from 'react-redux';

import './Shop-scene.css';

class Shop extends Component {
  render() {
    const { showModal } = this.props;
    return(
      <div>
        <Controls />
        <CarList />
        {showModal && <CreatingCarModal />}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  showModal: state.cars.showModal,
});

export default connect(mapStateToProps)(Shop);