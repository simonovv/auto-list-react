import React, { Component } from 'react';
import { connect } from 'react-redux';
import { togleModal, sortCars, getAllMarks, selectMark, selectName, getAllNames } from '../../store/actions/cars';

import './Controls.css';

import newCarIcon from '../../assets/plus.svg';
import sortPriceASC from '../../assets/sort-by-numeric-order.svg';
import sortPriceDESC from '../../assets/sort-by-order.svg';

class Controls extends Component {
  componentDidMount() {
    const { getAllMarks, getAllNames } = this.props;
    getAllMarks();
    getAllNames();
  }

  selectMarkHandler = event => {
    const { selectMark } = this.props;
    selectMark(event.target.value);
  }

  selectNameHandler = event => {
    const { selectName } = this.props;
    selectName(event.target.value);
  }

  togleModaWindowHandler = () => {
    const { togleModal } = this.props;
    togleModal();
  }

  sortASCCarsHandler = () => {
    const { sortCars } = this.props;
    sortCars('ASC');
  }

  sortDESCCarsHandler = () => {
    const { sortCars } = this.props;
    sortCars('DESC');
  }

  render() {
    const { marks, names } = this.props;
    return(
      <div className='controls'>
        <img className='icon' src={newCarIcon} onClick={this.togleModaWindowHandler} alt='New car'/>
        <img className='icon' src={sortPriceASC} onClick={this.sortASCCarsHandler} alt='SortASC'/>
        <img className='icon' src={sortPriceDESC} onClick={this.sortDESCCarsHandler} alt='SortDESC'/>
        {marks && (
          <select className='select-mark' onChange={this.selectMarkHandler}>
            <option value='All'>All marks</option>
            {marks.map(el => <option key={el.id} value={el.mark}>{el.mark}</option>)}
          </select>
        )}
        {names && (
          <select className='select-mark' onChange={this.selectNameHandler}>
            <option value='All'>All names</option>
            {names.map(el => <option key={el.id} value={el.name}>{el.name}</option>)}
          </select>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  marks: state.cars.marks,
  names: state.cars.names,
});

const mapDispatchToProps = dispatch => ({
  togleModal: () => dispatch(togleModal()),
  sortCars: (type) => dispatch(sortCars(type)),
  getAllMarks: () => dispatch(getAllMarks()),
  selectMark: mark => dispatch(selectMark(mark)),
  getAllNames: () => dispatch(getAllNames()),
  selectName: name => dispatch(selectName(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Controls);