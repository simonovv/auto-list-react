import * as actionTypes from '../actions/actionTypes';
import staticCarData from '../../StaticCarData';

const initialState = {
  cars: staticCarData,
  marks: [],
  names: [],
  showModal: false,
  selectedMark: 'All',
  selectedName: 'All',
}

const addNewCar = (state, action) => {
  const newCar = action.car;
  newCar.id = state.cars.length + 1;
  return {
    ...state,
    marks: [...state.marks],
    names: [...state.names],
    cars: [...state.cars, newCar]
  }
}

const togleModal = state => ({
  ...state,
  marks: [...state.marks],
  names: [...state.names],
  cars: [...state.cars],
  showModal: !state.showModal
});

const togleEditing = (state, action) => {
  const newState = { ...state, cars: [...state.cars], names: [...state.names] };
  const index = newState.cars.findIndex(e => e.id === action.id);
  newState.cars[index].isEditing ? delete newState.cars[index].isEditing : newState.cars[index].isEditing = true;
  return newState;
};

const updateCarInfo = (state, action) => {
  const newState = { ...state, cars: [...state.cars], names: [...state.names] };
  const index = newState.cars.findIndex(e => e.id === action.id);
  newState.cars[index] = { id: action.id, ...action.car }
  return newState;
};

const sortCars = (state, action) => {
  const newState = { ...state, cars: [...state.cars], names: [...state.names] };
  if(action.sortType === 'ASC')
    newState.cars.sort((a, b) => a.price - b.price);
  else if(action.sortType === 'DESC')
    newState.cars.sort((a, b) => b.price - a.price);
  return newState;
};

const getAllMarks = (state) => {
  const newState = { ...state, cars: [...state.cars], marks: [...state.marks], names: [...state.names] };
  newState.cars.forEach(el => {
    if(newState.marks.findIndex((element) => element.mark === el.mark) === -1)
      newState.marks.push({id: el.id, mark: el.mark})
  });
  return newState;
};

const getAllNames = (state) => {
  const newState = { ...state, cars: [...state.cars], marks: [...state.marks], names: [...state.names] };
  newState.cars.forEach(el => {
    if(newState.names.findIndex((element) => element.name === el.name) === -1)
      newState.names.push({id: el.id, name: el.name})
  });
  return newState;
};

const selectMark = (state, action) => ({
  ...state,
  cars: [...state.cars],
  marks: [...state.marks],
  names: [...state.names],
  selectedMark: action.mark,
});

const selectName = (state, action) => ({
  ...state,
  cars: [...state.cars],
  marks: [...state.marks],
  names: [...state.names],
  selectedName: action.name,
});

export default (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.SELECT_NAME: return selectName(state, action);
    case actionTypes.GET_ALL_NAMES: return getAllNames(state);
    case actionTypes.SELECT_MARK: return selectMark(state, action);
    case actionTypes.GET_ALL_MARKS: return getAllMarks(state);
    case actionTypes.SORT_CARS: return sortCars(state, action);
    case actionTypes.UPDATE_CAR_INFO: return updateCarInfo(state, action);
    case actionTypes.TOGLE_EDITING: return togleEditing(state, action);
    case actionTypes.TOGLE_CAR_MODAL: return togleModal(state);
    case actionTypes.ADD_NEW_CAR: return addNewCar(state, action);
    default: return state;
  }
};