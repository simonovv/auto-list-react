export const ADD_NEW_CAR = 'ADD_NEW_CAR';
export const TOGLE_CAR_MODAL = 'SHOW_CREATING_CAR_MODAL';
export const TOGLE_EDITING = 'TOGLE_EDITING';

export const UPDATE_CAR_INFO = 'UPDATE_CAR_INFO';

export const SORT_CARS = 'SORT_CARS';

export const GET_ALL_MARKS = 'GET_ALL_MARKS';
export const GET_ALL_NAMES = 'GET_ALL_NAMES';

export const SELECT_MARK = 'SELECT_MARK';
export const SELECT_NAME = 'SELECT_NAME';