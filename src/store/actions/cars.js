import * as actionTypes from './actionTypes';

export const addNewCar = car => ({
  type: actionTypes.ADD_NEW_CAR,
  car,
});

export const togleModal = () => ({
  type: actionTypes.TOGLE_CAR_MODAL,
});

export const togleEditing = id => ({
  type: actionTypes.TOGLE_EDITING,
  id,
});

export const updateCarInfo = (id, car) => ({
  type: actionTypes.UPDATE_CAR_INFO,
  id,
  car,
});

export const sortCars = sortType => ({
  type: actionTypes.SORT_CARS,
  sortType,
});

export const getAllMarks = () => ({
  type: actionTypes.GET_ALL_MARKS,
});

export const getAllNames = () => ({
  type: actionTypes.GET_ALL_NAMES,
});

export const selectMark = mark => ({
  type: actionTypes.SELECT_MARK,
  mark,
});

export const selectName = name => ({
  type: actionTypes.SELECT_NAME,
  name,
});