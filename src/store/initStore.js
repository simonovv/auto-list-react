import { createStore, combineReducers } from 'redux';
import reducers from './reducers/index';

const rootReducer = combineReducers({
  cars: reducers.cars,
});

export default createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());